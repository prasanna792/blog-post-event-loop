# Event loop


### To understand the event loop we need to know the Synchronous and Asynchronous

### 1. Synchronous 
* Synchronous operations are executed line by line,one after the other and the code waits for each operation to finish before moving onto the next.
* In the below code, the code executes line by line. So, the output is in the same order as the code written.
* So, JavaScript is single-threaded,it executes code one line at a time.

```
console.log("start")

function even(){
  console.log("inside function")
}
even();

console.log("end");

// Output: 
// start
// inside function
// end

```


### 2. Asynchronous:
* Asynchronous operations do not block the code's execution. They run separately and the code continues executing without waiting for them to complete.
* We can handle multiple tasks simultaneously. Instead of waiting for one task to finish, it can start another task and return to the first one later.

```
function asyncFunction() {

  console.log("Asynchronous function start");

  setTimeout(function() {
    console.log("Async function timeout done");
  }, 2000);

  console.log("Asynchronous function end");
}

asyncFunction()

// Output:
// Asynchronous function start
// Asynchronous function end
// Async function timeout done
```
* When you call `asyncFunction`, it executes asynchronously because of the `setTimeout`. The `setTimeout` function tells JavaScript to wait for 2 seconds before executing the function inside it.
* While waiting for those 2 seconds, the rest of the code continues to execute.
* So, `Asynchronous function start` and `Asynchronous function end` are printed immediately without waiting for the timeout.
* After 2 seconds, the timeout inside `asyncFunction` completes, and `Async function timeout done` is printed.
* This `non-blocking behavior` allows JavaScript to suitale for handling asynchronous operations.


## Event Loop
* Event loop allows to manage asynchronous operations and maintain its non-blocking behaviour. It continously monitors the `execution stack(call stack)` and the `callback queue` for tasks.

!["event loop representation"](https://www.javascripttutorial.net/wp-content/uploads/2019/12/javascript-event-loop-step-2.png)

!["event loop representation"](https://res.cloudinary.com/slawinski-dev/image/upload/v1648757381/javascript-runtime-environment.png)

### Call Stack :
* The call stack is a data structure in JavaScript. It keeps track of function calls during code execution.
* Whenever a function is called, it is added to the top of the call stack and when the function finishes executing,it is removed from the stack.
* The call stack follows the Last-In-First-Out(LIFO)principle,it means that the last function goes in is the first to come out.

```
function lakshmi() {
  console.log("Hello from lakshmi");
}

function prasanna() {
  console.log("Hello from prasanna!");
  lakshmi();
}

prasanna();

```

1. When the progra, starts executing, the initial call stack is empty.

          Call Stack: (empty)

2. The `prasanna()` function is called:

        Call Stack: prasanna()
3. Inside the `prasanna()` function, there is a call to the `lakshmi()` function:

        Call Stack: prasanna() -> lakshmi()

4. The `lakshmi()` function is called :

        Call Stack: prasanna() -> lakshmi()

5. Inside the `lakshmi()` function, there is a call to `console.log()`, it is a built-in function and does not affect the call stack.
6. The `lakshmi()` function finishes executing and returns to the `prasanna()` function:

         Call Stack: prasanna()   

7. Inside the `prasanna()` function, the next line after the call to `lakshmi()` is `console.log("Hello from prasanna!")`.This line is executed next.
8. The `prasanna()` function finishes executing and returns to the global scope

       Call Stack: (empty)

* At this point, the call stack is empty again,as all the function calls have been completed.             



### Some of the Web Browser Api's :
* **setTimeout() :** 
    * A function that schedules the execution of a callback function after a specified delay in milliseconds. 
* **DOM :** 
   * It allows manipulation and interaction with HTML documents. 
* **fetch() :** 
     * It is sued for making network requests.
* **Web Storage API :** 
    * It consists of localStorage and sessionStorage for storing key-value pairs on the client-side.
* **console.log() :** 
    * It is used to print 
* **GeoLocation :**
    * It provides access to the user's geographical location through the browser.
* **History :** 
     * It allows interacting with the browser's session history,helps in smooth navigation without page reloads.

### Callback Queue :
* The callback queue is a data structure which is used in event loop.It is a queue which consists of collection of tasks(callbacks).Those callbacks waits to be execute in a order.
* The callbacks are added to the queue when asynchronous operation are called or triggered.
* The callback queue  follows `first-in-first-out` manner. The tasks or callbacks are executed in the order they added to the queue.
* If the callstack is empty, event loop checks the callback queue for pending tasks to execute one by one.

```

function asyncCallbackOperation(time, callback) {
  setTimeout(() => {
    const result = "Hello timeout"
    callback(result);
  }, time);
}


function callback(result) {
  console.log(`callback result: ${result}`);
}

console.log("Start");

asyncCallbackOperation(0, callback); 
asyncCallbackOperation(1000, callback); 
asyncCallbackOperation(2000, callback); 

console.log("End");

// Output:
// Start
// End
// callback result: Hello timeout
// callback result: Hello timeout
// callback result: Hello timeout

```

* `Start` is printed first as it is a synchronous operation.
* asyncCallbackOperation called three times,so there are three tasks with that function but with different time. Those tasks are added to the callback queue. 
* Those callback tasks executed later,but event loop doesn't block the code to execute,so `End` is printed.
* If the callstack is empty,after 0ms the event loop takes the first asynchronous task to the callstack to execute.`Hello timeout` string is passed to the `callback` function. The `callback` function is added to the callback queue.
* After 1000ms,the second asynchronous task completes and the `Hello timeout` string is passed to the `callback` function. The `callback` function is added to the callback queue.
* After 2000ms, the third asynchronous task completes and the `Hello timeout` string is passed to the `callback` function again.The `callback` function is added to the callback queue.
* The event loop checks that the call stack is empty or not.Now, it executes the tasks one by one from the callback queue.
* The `callback` function is executed for each task and it gives result as three times `Callback result:  Hello timeout`.




### Micro Task Queue :

* The micro task queue also known as the Job Queue or Promise Queue. It is a queue tht stores micro tasks.
* Micro task queue have higherpriority than regular asynchronous tasks like `setTimeout` callbacks.
* Micro tasks include Promise callbacks, mutation observer callbacks and other tasks that need to be executed.
* The event loop checks the micro task queue when the call stack becomes empty and beforechecking the regular task queue.
* It means that micro tasks get executed before regular tasks.

```
console.log("Start");

setTimeout(() => {
    console.log("hello timer");
},0)

function promise(){
    return new Promise((resolve,reject) => {
        resolve("Micro task executed")
    })
}

promise()
.then((result) => console.log(result))
.catch((err) => console.error(err))

console.log("End");

```

* The call stack starts with the global context and executes the synchronous code in the order it appears.
* After executing the synchronous code, the call stack is empty, and the event loop checks the micro task queue.
* The `promise().then()` micro task is in the micro task queue, so its callback is taken and executed. It logs "Micro task executed" to the console.
* After executing the micro task, the event loop checks the task queue. The `setTimeout` callback is in the callback or task queue, so it is taken and executed. It prints "hello timer".
* Even though the `setTimeout` set to 0 milliseconds, it was considered as a task, on the other hand the promise's micro task had a higher priority and got executed first.


### Example of Event loop

```
console.log("Start");

// First setTimeout callback
setTimeout(function () {
  console.log("Timeout 1 executed");
}, 0);

// Promise
Promise.resolve().then(function () {
  console.log("Promise 1 executed");
});

// Second setTimeout callback
setTimeout(function () {
  console.log("Timeout 2 executed");
}, 0);

console.log("End");

```
!["start prints to the console"](../images/start.png)
!["first setTimeout"](../images/timeout-1.png)
!["setTimeout is a web api so it will be store in callback queue to wait it's turn to execute"](../images/web-api-1.png)
!["promise executes before the setTimeout values"](../images/promise.png)
!["callstack takes second timeout and the event loop place it in callback queue"](../images/timer-2-1.png)
!["first timeout value will get executed"](../images/timer-2.png)


* When the code starts executing, the initial `console.log("Start")` statement is executed immediately and "Start" is printed to the console.
* Now, the first `setTimeout` callback is called. The `setTimeout` function is a web browser API, so it's taken out of the main thread and scheduled to run after 0 milliseconds.The remaining synchronous code continues to execute.
* Next, we have a Promise using `Promise.resolve()`. A Promise is a micro task, which means it has higher priority than regular tasks like `setTimeout`. The `then` method is called on the Promise, and the micro task is added to the micro task queue.
* After that, we encounter the second `setTimeout` callback.Like the first one, it is also scheduled to run after 0 milliseconds, but it doesn't execute immediately. The rest of the synchronous code continues.
* The last `console.log("End")` statement is executed and "End" is printed to the console.

Now, the synchronous code execution is complete and the call stack is empty.

1. The event loop checks the micro task queue first. It finds the Promise's `then` callback, and `Promise 1 executed` is printed to the console.
2. The event loop then checks the web browser API tasks(regular task queue). It finds the first `setTimeout` callback and executes `Timeout 1 executed`.
3. Next,the event loop again checks the web browser API tasks and finds the second `setTimeout` callback, executing `Timeout2 executed`



### Summary

* The event loop in JavaScript is like a traffic cop that manages the execution of tasks.
* The time-consuming tasks are handled separately,so that they don't block the main thread.
* This way,the event loop in JavaScript manages the execution of asynchronous tasks by using a task queue(Callback queue),microtask queue,call stack and web apis.